package com.example.shubhraj.courtlink.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.example.shubhraj.courtlink.models.Request;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.example.shubhraj.courtlink.constants.Constants.REQUEST_ID;

/**
 * Created by Shubhraj on 07-11-2017.
 */

public class RequestUpload extends IntentService
{
    private FirebaseDatabase database;
    private DatabaseReference mDbReference;

    public RequestUpload() {
        super(RequestUpload.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Request request = (Request) intent.getSerializableExtra(REQUEST_ID);
        database = FirebaseDatabase.getInstance();
        mDbReference = database.getReference("ongoing_requests");
        String key = mDbReference.push().getKey();/*
        mDbReference.child(key).setValue(new Request(request.getCaseHeadline(),request.getCaseDescription(),
                request.getCaseDate(),request.getNameOfRequester(),request.getNameOfCourt(),request.getMobileNumber(), caseType));*/
    }
}
