package com.example.shubhraj.courtlink.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.shubhraj.courtlink.R;
import com.example.shubhraj.courtlink.adapters.CaseListAdapter;
import com.example.shubhraj.courtlink.models.Request;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedFragment extends Fragment
{
    private ArrayList<Request> completedRequests;
    @BindView(R.id.completed_rv)
    RecyclerView completedRv;
    @BindView(R.id.download_layout)
    LinearLayout progressUpdate;

    private FirebaseDatabase database;
    private DatabaseReference mDbReference;
    private CaseListAdapter adapter;

    public CompletedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completed, container, false);
        ButterKnife.bind(this, view);
        /*initializeData();*/
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        completedRv.setHasFixedSize(true);
        completedRv.setLayoutManager(layoutManager);
        database = FirebaseDatabase.getInstance();
        mDbReference = database.getReference("completed_requests");

        initializeList();
        adapter = new CaseListAdapter(this.getContext(), completedRequests);
        completedRv.setAdapter(adapter);

        return view;
    }

    private void initializeList()
    {
        completedRequests = new ArrayList<>();
        mDbReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                Request request = dataSnapshot.getValue(Request.class);
                completedRequests.add(request);
                Log.d("Shubhraj",""+completedRequests.size());
                adapter.notifyDataSetChanged();
                progressUpdate.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
