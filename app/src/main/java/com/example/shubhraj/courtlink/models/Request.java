package com.example.shubhraj.courtlink.models;

import java.io.Serializable;

/**
 * Created by Shubhraj on 04-11-2017.
 */

public class Request implements Serializable
{
    private String caseHeadline, caseDescription, caseDate,
            nameOfRequester, nameOfCourt, mobileNumber, caseType;

    public Request() {
    }

    public Request(String caseHeadline, String caseDescription, String caseDate,
                   String nameOfRequester, String nameOfCourt, String mobileNumber, String caseType) {
        this.caseHeadline = caseHeadline;
        this.caseDescription = caseDescription;
        this.caseDate = caseDate;
        this.nameOfRequester = nameOfRequester;
        this.nameOfCourt = nameOfCourt;
        this.mobileNumber = mobileNumber;
        this.caseType = caseType;
    }

    public String getCaseHeadline() {
        return caseHeadline;
    }

    public void setCaseHeadline(String caseHeadline) {
        this.caseHeadline = caseHeadline;
    }

    public String getCaseDescription() {
        return caseDescription;
    }

    public void setCaseDescription(String caseDescription) {
        this.caseDescription = caseDescription;
    }

    public String getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(String caseDate) {
        this.caseDate = caseDate;
    }

    public String getNameOfRequester() {
        return nameOfRequester;
    }

    public void setNameOfRequester(String nameOfRequester) {
        this.nameOfRequester = nameOfRequester;
    }

    public String getNameOfCourt() {
        return nameOfCourt;
    }

    public void setNameOfCourt(String nameOfCourt) {
        this.nameOfCourt = nameOfCourt;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }
}
