package com.example.shubhraj.courtlink.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.shubhraj.courtlink.R;
import com.example.shubhraj.courtlink.activities.RequestDetailsActivity;
import com.example.shubhraj.courtlink.models.Request;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.shubhraj.courtlink.constants.Constants.REQUEST_ID;

/**
 * Created by Shubhraj on 04-11-2017.
 */

public class CaseListAdapter extends RecyclerView.Adapter<CaseListAdapter.ViewHolder>
{
    private Context mContext;
    private ArrayList<Request> requests;

    public CaseListAdapter(Context mContext, ArrayList<Request> requests) {
        this.mContext = mContext;
        this.requests = requests;
    }

    @Override
    public CaseListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_request_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CaseListAdapter.ViewHolder holder, int position) {
        Request request = requests.get(position);
        holder.titleText.setText(request.getCaseHeadline());
        holder.dateText.setText(request.getCaseDate());
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.headline_tv)
        TextView titleText;
        @BindView(R.id.date_tv)
        TextView dateText;
        @BindView(R.id.card_view)
        CardView cv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Request request = requests.get(getAdapterPosition());
            Intent intent = new Intent(mContext, RequestDetailsActivity.class);
            intent.putExtra(REQUEST_ID, request);
            mContext.startActivity(intent);
        }
    }
}
