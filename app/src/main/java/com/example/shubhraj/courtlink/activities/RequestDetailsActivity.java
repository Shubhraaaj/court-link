package com.example.shubhraj.courtlink.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.shubhraj.courtlink.R;
import com.example.shubhraj.courtlink.models.Request;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.shubhraj.courtlink.constants.Constants.REQUEST_ID;

public class RequestDetailsActivity extends AppCompatActivity
{
    @BindView(R.id.request_headline)
    TextView headlineText;
    @BindView(R.id.request_description)
    TextView descriptionText;
    @BindView(R.id.applier_details)
    TextView detailsText;
    @BindView(R.id.request_type)
    TextView requestType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        ButterKnife.bind(this);
        setTitle(getString(R.string.request_details_label));
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            Request request = (Request) extras.getSerializable(REQUEST_ID);
            headlineText.setText(request.getCaseHeadline());
            descriptionText.setText(request.getCaseDescription());
            detailsText.setText(request.getNameOfRequester()+"\n"+request.getMobileNumber()+"\n"
                    +request.getCaseDate());
            String selection = request.getCaseType();

            if (selection.equals("aadhar_card"))
            {
                requestType.setText("AADHAR Card");
            }
            else if (selection.equals("affidavit_label"))
            {
                requestType.setText("Affidavit");
            }
            else if (selection.equals("arms_licence_label"))
            {
                requestType.setText("Arms License");
            }
            else if (selection.equals("bail_papers"))
            {
                requestType.setText("Bail Papers");
            }
            else if (selection.equals("driving_licence_label"))
            {
                requestType.setText("Driving License");
            }
            else if (selection.equals("land_registration"))
            {
                requestType.setText("Land Registration");
            }
            else if (selection.equals("notary"))
            {
                requestType.setText("Notary");
            }
            else if (selection.equals("stamp_paper"))
            {
                requestType.setText("Stamp Paper");
            }
            else if (selection.equals("voter_id"))
            {
                requestType.setText("Voter ID");
            }
            else if (selection.equals("land_accusation"))
            {
                requestType.setText("Land Accusation");
            }
            else if (selection.equals("business_registration"))
            {
                requestType.setText("Business Registration");
            }
            else if (selection.equals("divorce_label"))
            {
                requestType.setText("Divorce Papers");
            }
            else if (selection.equals("others"))
            {
                requestType.setText("Other");
            }
            else
            {
                requestType.setText("Uncategorised");
            }
        }
    }
}
