package com.example.shubhraj.courtlink.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.shubhraj.courtlink.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ContactusActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        String hashSequence = "ashu.shubhraj@gmail.com860386208526121995";
        String serverCalculatedHash= hashCal("SHA-512", hashSequence);
    }
    public static String hashCal(String type, String hashString) {
        StringBuilder hash = new StringBuilder();
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(type);
            messageDigest.update(hashString.getBytes());
            byte[] mdbytes = messageDigest.digest();
            for (byte hashByte : mdbytes) {
                hash.append(Integer.toString((hashByte & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash.toString();
    }
}
