package com.example.shubhraj.courtlink.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shubhraj.courtlink.R;
import com.example.shubhraj.courtlink.models.Request;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateRequestActivity extends AppCompatActivity
{
    @BindView(R.id.date_textView)
    TextView dateText;
    @BindView(R.id.case_headline)
    EditText caseHeadline;
    @BindView(R.id.applicant_name)
    EditText applicantName;
    @BindView(R.id.case_type_spinner)
    Spinner caseType;
    @BindView(R.id.case_description)
    EditText caseDescription;
    @BindView(R.id.applicant_number)
    EditText applicantPhone;
    @BindView(R.id.linear_layout)
    LinearLayout linearLayout;
    @BindView(R.id.upload_layout)
    LinearLayout uploadLayout;
    @BindView(R.id.court_name)
    EditText courtName;

    private FirebaseDatabase database;
    private DatabaseReference mDbReference;
    private String headline, court, name, description, phone, requestType,
            currentDateTimeString, billDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_request);
        ButterKnife.bind(this);
        setupSpinner();
        uploadLayout.setVisibility(View.GONE);
        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        dateText.setText("Date - " + currentDateTimeString.substring(0,11));

        database = FirebaseDatabase.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.request_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_done)
        {
            headline = caseHeadline.getText().toString();
            name = applicantName.getText().toString();
            description = caseDescription.getText().toString();
            phone = applicantPhone.getText().toString();
            court = courtName.getText().toString();
            if ((headline.length()>0)&&(name.length()>0)&&(description.length()>0)
                    &&(phone.length()>0)&&(court.length()>0))
            {
                createConfirmDialog();
            }
            else
            {
                Toast.makeText(this,"Please fill all the details", Toast.LENGTH_SHORT).show();
            }
        }
        if (id == android.R.id.home)
        {
            onBackPressed();
        }
        return true;
    }

    private void createConfirmDialog() {
        String message = billDetails+"\n\nHeadline - " + headline +"\n\nApplicant - "
                + name + "\n\nDescription - " + description +"\n\nPhone - " + phone +"\n\nType - "+ requestType
                +"\n\nCourt - " + court;
        final AlertDialog.Builder builder = new AlertDialog.Builder(CreateRequestActivity.this);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                createUploadTask();
                intentHomeActivity();
                /*Intent uploadIntent = new Intent(CreateRequestActivity.this, RequestUpload.class);
                uploadIntent.putExtra(REQUEST_ID, new Request(headline,description,
                        currentDateTimeString.substring(0,11),name,court,phone));
                startService(uploadIntent);*/
            }
        });
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void intentHomeActivity() {
        Intent intent = new Intent(CreateRequestActivity.this, PaymentActivity.class);
        startActivity(intent);
    }

    private void createUploadTask() {
        linearLayout.setVisibility(View.GONE);
        mDbReference = database.getReference("ongoing_requests");
        String key = mDbReference.push().getKey();
        mDbReference.child(key).setValue(new Request(headline,description,
                currentDateTimeString.substring(0,11),name,court,phone, requestType));
        Toast.makeText(CreateRequestActivity.this, "Successfully uploaded",Toast.LENGTH_SHORT).show();
    }

    private void setupSpinner()
    {
        ArrayAdapter requestAdapter = ArrayAdapter.createFromResource(this,
                R.array.request_type, android.R.layout.simple_spinner_item);

        requestAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        caseType.setAdapter(requestAdapter);

        caseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.aadhar_card)))
                    {
                        requestType = "aadhar_card";
                        billDetails = "Your AADHAR Card will be delivered in 7 working days";
                    }
                    else if (selection.equals(getString(R.string.affidavit_label)))
                    {
                        requestType = "affidavit_label";
                        billDetails = "You will receive your Affidavit within 2 working days";
                    }
                    else if (selection.equals(getString(R.string.arms_licence_label)))
                    {
                        requestType="arms_licence_label";
                        billDetails = "Our agent will contact you regarding your arms license within 2 hours";
                    }
                    else if (selection.equals(getString(R.string.bail_papers)))
                    {
                        requestType = "bail_papers";
                        billDetails = "Worry not, We will get you bailed within 24 Hours";
                    }
                    else if (selection.equals(getString(R.string.driving_licence_label)))
                    {
                        requestType = "driving_licence_label";
                        billDetails = "Our agent will contact you regarding your driving license within 2 hours";
                    }
                    else if (selection.equals(getString(R.string.land_registration)))
                    {
                        requestType = "land_registration";
                        billDetails = "We will put forward the Registration process and let you know.";
                    }
                    else if (selection.equals(getString(R.string.notary)))
                    {
                        requestType="notary";
                        billDetails = "We will get you the Notary bill within 2 working days";
                    }
                    else if (selection.equals(getString(R.string.stamp_paper)))
                    {
                        requestType = "stamp_paper";
                        billDetails = "You will get your Stamp Papers delivered to you within 2-working days";
                    }
                    else if (selection.equals(getString(R.string.voter_id)))
                    {
                        requestType="voter_id";
                        billDetails = "Our agent will contact you for the details in 2 hours";
                    }
                    else if (selection.equals(getString(R.string.land_accusation)))
                    {
                        requestType = "land_accusation";
                        billDetails = "Thank you for posting. Our consultancy team will contact you in an hours";
                    }
                    else if (selection.equals(getString(R.string.business_registration)))
                    {
                        requestType = "business_registration";
                        billDetails = "Thank you for posting. Our consultancy team will contact you in two hours";
                    }
                    else if (selection.equals(getString(R.string.divorce_label)))
                    {
                        requestType = "divorce_label";
                        billDetails = "We will send the Divorce notice within 24 hours";
                    }
                    else if (selection.equals(R.string.others))
                    {
                        requestType = "other";
                        billDetails = "We will contact you within an hour";
                    }
                    else
                    {
                        requestType = "other";
                        billDetails = "We will contact you within an hour";
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                requestType = "aadhar_card";
            }
        });
    }

}

