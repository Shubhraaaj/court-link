package com.example.shubhraj.dreams.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.example.shubhraj.courtlink.R;

/**
 * Created by Shubhraj on 03-11-2017.
 */

public class BlueButton extends AppCompatButton
{
    private Context mContext;

    public BlueButton(Context context) {
        super(context);
        init(context);
    }

    public BlueButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BlueButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    public void init(Context context)
    {
        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(isPressed())
        {
            setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_btn_pressed, null));
        }
        else
        {
            setBackgroundDrawable(getResources().getDrawable(R.drawable.blue_btn_bckgrnd, null));
        }
        setTextColor(Color.parseColor("#eceff1"));
        super.onDraw(canvas);
    }
}
